var nodeSass = require('node-sass');

var app = new EmberApp({
  sassOptions: {
    implementation: nodeSass,
  },
});
