import Component from '@glimmer/component';

export default class CarouselCharacteristics extends Component {
  characteristics = [
    '👨‍💻 Développeur Web 🖥',
    '⚡️ Électromécanicien 🔧',
    '🧘🏼 Sportif 🏃',
    '🎪 Jongleur 🤹',
    '🔩 Bricoleur 👨‍🔧',
    "🕺🏼 Et encore plein d'autre chose ... 👨🏻‍🍳",
  ];
}
