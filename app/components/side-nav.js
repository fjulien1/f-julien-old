import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { mobileAndTabletCheck } from '../helpers/responsive';

const hiddenMenu = 'hidden-menu';

export default class SideNavComponent extends Component {
  @tracked isOpen = !mobileAndTabletCheck();
  @tracked menuDisplay = mobileAndTabletCheck() ? hiddenMenu : '';

  @action toggleOpen() {
    this.isOpen = !this.isOpen;
    this.menuDisplay = this.isOpen ? '' : hiddenMenu;
  }
  navigationLink = [
    { url: 'index', icon: 'home', word: 'accueil' },
    { url: 'a-propos-de-moi', icon: 'user-astronaut', word: 'à propos de moi' },
    { url: 'competences', icon: 'id-card', word: 'compétences' },
    { url: 'portfolio', icon: 'splotch', word: 'portfolio' },
    { url: 'contact', icon: 'phone-alt', word: 'contact' },
  ];
}
