import EmberRouter from '@ember/routing/router';
import config from 'fjulien/config/environment';

export default class Router extends EmberRouter {
  location = config.locationType;
  rootURL = config.rootURL;
}

Router.map(function () {
  this.route('a-propos-de-moi');
  this.route('competences');
  this.route('portfolio');
  this.route('contact');
});
