import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | a propos de moi', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:a-propos-de-moi');
    assert.ok(route);
  });
});
