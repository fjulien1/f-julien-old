import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

module('Integration | Component | carousel-words', function (hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function (assert) {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.set('myAction', function(val) { ... });

    await render(hbs`<CarouselCharacteristics />`);
    const textResult =
      "👨‍💻-Développeur-Web-🖥--------⚡️-Électromécanicien-🔧--------🧘🏼-Sportif-🏃--------🎪-Jongleur-🤹--------🔩-Bricoleur-👨‍🔧--------🕺🏼-Et-encore-plein-d'autre-chose-...-👨🏻‍🍳";
    assert.equal(
      this.element.textContent.trim().replace(/\s/g, '-'),
      textResult
    );

    // Template block usage:
    await render(hbs`

      <CarouselCharacteristics/>
    `);

    assert.equal(
      this.element.textContent.trim().replace(/\s/g, '-'),
      textResult
    );
  });
});
