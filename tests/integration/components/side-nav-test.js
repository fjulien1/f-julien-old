import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

module('Integration | Component | NavBar', function (hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function (assert) {
    await render(hbs`<SideNav />`);
    assert.equal(
      this.element.textContent.replace(/\r?\n|\r|\s/g, ''),
      'accueilàproposdemoicompétencesportfoliocontact'
    );
  });
});
