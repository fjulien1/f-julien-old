import { module, test } from 'qunit';
import { click, visit, currentURL } from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';

module('Acceptance | fjulien', function (hooks) {
  setupApplicationTest(hooks);

  test('Visiting /', async function (assert) {
    await visit('/');

    // Element is present
    assert.dom('nav').exists();
    assert.dom('.side-nav').exists();
    assert.dom('.main').exists();

    // Contents is present
    assert.dom('.menu-index').hasText('accueil');
    assert.dom('.menu-a-propos-de-moi').hasText('à propos de moi');
    assert.dom('.menu-competences').hasText('compétences');
    assert.dom('.menu-portfolio').hasText('portfolio');
    assert.dom('.menu-contact').hasText('contact');

    assert.equal(currentURL(), '/');
  });

  test('Navigating using the side-nav', async function (assert) {
    await visit('/');

    await click('.menu-index');
    assert.equal(currentURL(), '/');

    await click('.menu-a-propos-de-moi');
    assert.equal(currentURL(), '/a-propos-de-moi');

    await click('.menu-portfolio');
    assert.equal(currentURL(), '/portfolio');

    await click('.menu-contact');
    assert.equal(currentURL(), '/contact');
  });
});
